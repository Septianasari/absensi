<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Absen;
use Auth;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('home');
    }

    public function absen(Request $request)
    {
        $user_id = Auth::user()->id;
        $date = date("Y-m-d");    
        $time = date("H:i:s");
        $note = $request->note;

        $absen = new Absen;
        if (isset($request->btnIn)) {
            $absen->create([
                'id' => $user_id,
                'date' => $date,
                'time_in' => $time,
                'note' => $note
            ]);
            return redirect()->back();
        } elseif (isset($request->btnOut)) {
            return "absen keluar";
        }

        return $request->all(); 
    }

}
