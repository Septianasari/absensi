@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <h4 class="panel-heading">Informasi</h4>

                <div class="panel-body">
                    <table class="table-responsive table-bordered">
                        <form action="/absen" method="post">
                            {{ csrf_field() }}
                            <tr>
                                <td>
                                    <input type="text" class="form-control" name="note" placeholder="Keterangan" autocomplete="off" autofocus=""></textarea>
                                </td>
                                <td>
                                    <button type="submit" class="btn btn-inverse-dark btn-fw"  name="btnIn">Absen Masuk</button>
                                <td>
                                    <button type="submit" class="btn btn-inverse-dark btn-fw" name="btnOut">Absen Keluar</button>
                                </td>
                            </tr>
                         </form>
                    </table>
                </div>
            </div>
        </div>
    </div>
                
    <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Riwayat Absensi</h4>
                  <table class="table table-bordered">
                    <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Jam Masuk</th>
                                <th>Jam Keluar</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>2019-09-02</td>
                                <td>08:00:00</td>
                                <td>08:00:00</td>
                                <td>Hello</td>
                            </tr>
                        </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection
