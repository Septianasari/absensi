<?php

use Illuminate\Database\Seeder;

class userseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array([
        	'nip'=>'098765',
        	'nama_pegawai'=>'Lavina',
        	'email'=>'lavina@gmail.com',
        	'password'=>bcrypt('lavina123'),
        	'jenis_kelamin'=>'Perempuan',
        	'usia'=>'17',
        	'agama'=>'islam',
        	'tempat_lahir'=>'Magelang',
        	'tanggal_lahir'=>'1900-12-12',
        	'id_jabatan'=>'1',
        	'id_pendidikan'=>'1',
        	'tahun_masuk'=>'2018',
        	'no_telepon'=>'085772528097',
        	'alamat'=>'Jalan Sudirman',
        	'foto'=>'nchuehs.jpg'
        ]));
    }
}
